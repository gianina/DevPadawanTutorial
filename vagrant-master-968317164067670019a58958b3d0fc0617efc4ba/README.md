TODO
----

 - Download and install virtualbox (no configuration needed)
 - Download and install vagrant
 - Download repo's zip archive
 - Unpack the zip archive
 - On windows:
    - Install git from https://git-for-windows.github.io
    - Start the git shell and go to the directory that contains the 'Vagrantfile' file
 - Start the terminal/console application and run `vagrant up` in the directory that contains the 'Vagrantfile' file
 - Grab a coffee
 
 
 
 
 